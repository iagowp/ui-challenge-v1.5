var assert = require('assert');
var verifyURL = require('../js/verify_url.js').verifyURL;

describe('Array', function() {
  describe('verify_url', function() {
    it('should reject empty strings', function() {
      assert.equal(verifyURL(''), false);
    });

    it('should reject URLS with http', function() {
      assert.equal(verifyURL('http://globo.com'), false);
      assert.equal(verifyURL('https://globo.com'), false);
    });

    it('should reject urls with subdomains', function() {
      assert.equal(verifyURL('g1.globo.com.br'), false);
    });

    it('should reject urls without any dots', function() {
      assert.equal(verifyURL('asdasda'), false);
    });

    it('should accept urls starting with www', function() {
      assert.equal(verifyURL('www.globo.com'), true);
      assert.equal(verifyURL('www.globo.com.br'), true);
    });

    it('should accept starting with domain name', function() {
      assert.equal(verifyURL('globo.com'), true);
      assert.equal(verifyURL('globo.com.br'), true);
    });
  });
});