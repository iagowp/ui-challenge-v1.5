# Avenue Code UI Challenge - Part I #

### How to run project ###
* Clone repo
* Open html file

### How to run unit tests ###
* Install mocha globally
* run `mocha` from terminal on project's root folder

### Key decisions taken during project, and motivations ###
* When I was given this project, I was told it would only take about 2 hours, so I ended up deciding to do during the week. After asking for a deadline, I realized that in order for the project to be delivered with all functional and non-functional criteria, it would take me a lot more than 2 hours, as I didn't have the ammount of time to deliver a project I would be proud of on the deadline, I ended up taking lots of decisions to just be able to deliver the minimum in time.
* I decided to remove handlebars after taking longer than 15 minutes to figure out how to work with it, having the time in mind
* I decided not to use any web app framework more complex than jQuery as it was advised not to use any code/file/folder structure auto generator tools, and setting up would take a long time. Also, its a fairly simple project, so using something like Meteor sounds like overkill.
* I decided not to use a presentation framework because I didn't have the time to style the project, and the layout was simple enough to need to use frameworks like sass, that helps organizing/modularizing code
* I didn't add any task automation tool, as there was no task to automate (Not really, actually I could have minified and concatenated js/css, but no time)
* Overall, knowing the length of this project, I'd do it on weekend. There's lots of rooms for improvement on the nice to haves, but overall I believe the functionality is what was asked for