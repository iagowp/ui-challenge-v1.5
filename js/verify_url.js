function verifyURL (url) {
  if (url.includes('://')) return false
  if (url.startsWith('www.')) return true
  const divisions = url.split('.').length
  // avoids too short urls, and urls that starts with subdomains
  if (divisions > 3 || divisions <= 1) return false
  return true
}

var exports = exports || {}
exports.verifyURL = verifyURL;