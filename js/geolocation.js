// ================================================
// Note to candidate:
//	This code has bad practices
//	feel free to replace it with a better solution
// ================================================
$(document).ready(function(){
	var API_HOST = "freegeoip.net";
	var now;
	var hostMoment;
	var isUserTableUsed = false;
	var isHostTableUsed = false;

	initializeButtons();

	function updateLocationDetails(data, divId = '') {
		if(divId){
			hostMoment = new Date()
		}else{
			now = new Date();
		}

		$("#" + divId + "location_query").html(data.ip);
		$("#" + divId + "location_country").html(data.country_name);
		$("#" + divId + "location_regionName").html(data.region_name);
		$("#" + divId + "location_city").html(data.city);
		$("#" + divId + "location_zip_code").html(data.zip_code);
		$("#" + divId + "location_lat").html(data.latitude);
		$("#" + divId + "location_lon").html(data.longitude);

		$("table").removeClass("empty");
	}

	function initializeButtons() {
		$('#btnMyLocation').click(getMyLocation);

		$('#btnResetLocation').click(resetLocationDetails);

		$('#btnLocateHost').click(locateHost);

		$(".help").click(displayHelpInfo);
	}

	function displayHelpInfo(e) {
		var isUserTable = $(e.currentTarget).parents('#userTable').length;
		var moment = isUserTable ? now : hostMoment;
		var fieldName = $(e.currentTarget).closest('tr').find('.field_name').text();
		alert( "This is your " + fieldName + " according to " + API_HOST + " at " + moment );
	}

	function getMyLocation() {
		$.ajax({
			type: 'GET',
			url: 'http://' + API_HOST + '/json/',
			success : function(response){
				updateLocationDetails(response);
				isUserTableUsed = true
				$('#userTable').removeClass('hidden')
			}
		});
	}

	function resetLocationDetails() {
		updateLocationDetails({
			ip: "0.0.0.0",
			country_name: "",
			region_name: "",
			city: "",
			zip_code: "",
			latitude: "",
			longitude: ""
		});
		if(!isHostTableUsed){
			$("table").addClass("empty");
		}else{
			$('#userTable').addClass('hidden')
		}
		isUserTableUsed = false
	}

	function locateHost() {
		isHostTableUsed = true;
		const url = $('#hostInput').val()
		if (!verifyURL(url)) return alert('Invalid URL')

			$.ajax({
				type: 'GET',
				url: 'http://freegeoip.net/json/' + url,
				success: function(response){
					updateLocationDetails(response, 'host_');
					updateDOMVisibility()
				}
			})

		function updateDOMVisibility () {
			$('#hostLocation').removeClass('hidden');
			$("table").removeClass("empty");
			if(!isUserTableUsed){
				$('#userTable').addClass('hidden');
			}
		}
	}
});
